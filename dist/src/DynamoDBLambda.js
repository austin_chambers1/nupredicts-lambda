var aws = require('aws-sdk');
aws.config.region = "us-east-1";
//var db = new aws.DynamoDB({ endpoint: new aws.Endpoint('http://localhost:8000') });
var db = new aws.DynamoDB();

var docClient = new aws.DynamoDB.DocumentClient()


function route(event, context, callback){
// Grabs all of the items from the Votes table and sends it to the Aggregate table

if (event['operation'] === 'process_vote'){
function scanVotesData(){
	var params = {
	    TableName: "amps-nupredicts-votes",
	};

	console.log("Scanning Votes table.");
	docClient.scan(params, onScan);

	function onScan(err, data) {
	    if (err) {
	        console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
	        callback(err, null);
	    } else {
	        // print all the movies
	        console.log("Scan succeeded.");
			console.log(JSON.stringify(data, null, 2));
			console.log("Adding to aggregate table ...")
			console.log(data);
			addToAggregate(data);
			callback(null, data);
	        }

	        // continue scanning if we have more movies
	        // if (typeof data.LastEvaluatedKey != "undefined") {
	        //     console.log("Scanning for more...");
	        //     params.ExclusiveStartKey = data.LastEvaluatedKey;
	        //     docClient.scan(params, onScan);
	        // }
	}
}

// Sets up Aggregate table
function addToAggregate(data){
	var count = 0;
	var stats = statsCalculations(data);
	var details = "chchchchchch";
	data.Items.forEach(function(entry){
		var params = {
		    TableName: 'amps-nupredicts-voteaggregates',
		    Item:{
		    	"instance": count,
		        "topic": entry.topic,
		        "stats": stats,
		        "details": details
		    }
		};

		console.log("Adding a new item...");
		docClient.put(params, function(err, data) {
		    if (err) {
		        console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
		        callback(err, null);
		    } else {
		        console.log("Added item:", JSON.stringify(data, null, 2));
		        callback(null, data)

		    }
		});
		count++;
	});
	}

// Does stats calculations
// @TODO: decide which features we want
function statsCalculations(data){
	var stats = {};
	var yes_total = 0;
	var no_total = 0;
	var yes_percentage = 0;
	var no_percentage = 0;
	//console.log("YE:", data);

	data.Items.forEach(function(entry){
		if (entry.vote.answer[0] === 'yes'){
			yes_total++;
		}
		else if (entry.vote.answer[0] === 'no'){
			no_total++;
		}
		else{
			console.log(entry.vote.answer[0], 'is not a valid answer, not factoring in in calculations');
		}
	});

	yes_percentage = (yes_total / data.Count) * 100;
	no_percentage = (no_total / data.Count) * 100;

	stats = {"Percent_Yes": yes_percentage,
			 "Percent_No": no_percentage}

	return stats;
	}

	scanVotesData();
}
	else{
		console.log("Wrong Event Command");
	}
}

// var event = {
// 	'operation':'process_vote'
// }

// route(event, null, function(err, data){});

module.exports = {
	route: route
}

