var app = require("./src/DynamoDBLambda.js");

exports.handler = function(event, context, callback){
  console.log("EVENT", event);
  switch(event.operation){
    case "process_vote":
    app.route(event, context, callback);
    break;
    case "echo":
    callback(null, event);
    break;
    case "ping":
    callback(null, "pong");
    break;
    default:
    app.route(event, context, callback);
    break;
  }
}
