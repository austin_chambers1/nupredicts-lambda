var aws = require('aws-sdk');
aws.config.region = "us-east-1";
var db = new aws.DynamoDB();
var docClient = new aws.DynamoDB.DocumentClient()

// creates a table given either name Aggregate or Votes
function createTable(name){

	if (name === "Votes"){
		var params = {
		    TableName : "Votes",
		    KeySchema: [       
		        { AttributeName: "Topic", KeyType: "HASH"},  //Partition key
		        { AttributeName: "Netid", KeyType: "RANGE" }  //Sort key
		    ],
		    AttributeDefinitions: [       
		        { AttributeName: "Topic", AttributeType: "S" },
		        { AttributeName: "Netid", AttributeType: "S" }
		    ],
		    ProvisionedThroughput: {       
		        ReadCapacityUnits: 10, 
		        WriteCapacityUnits: 10
		    }
		};
	}

	else{
		var params = {
		    TableName : "Aggregate",
		    KeySchema: [       
		        { AttributeName: "Topic", KeyType: "HASH"},  //Partition key
		        { AttributeName: "Instance", KeyType: "RANGE" }  //Sort key
		    ],
		    AttributeDefinitions: [       
		        { AttributeName: "Topic", AttributeType: "S" },
		        { AttributeName: "Instance", AttributeType: "N" }
		    ],
		    ProvisionedThroughput: {       
		        ReadCapacityUnits: 10, 
		        WriteCapacityUnits: 10
		    }
		};
	}

	db.createTable(params, function(err, data) {
	    if (err) {
	        console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
	    } else {
	        console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
	    }
	});
}

// deletes a table given its name
function deleteTable(table){
	var params = {
	    TableName : table
	};

	db.deleteTable(params, function(err, data) {
	    if (err) {
	        console.error("Unable to delete table. Error JSON:", JSON.stringify(err, null, 2));
	    } else {
	        console.log("Deleted table. Table description JSON:", JSON.stringify(data, null, 2));
	    }
	});
}

// add data to a table
// @TODO: make this dynamic and loaded from a different file
function addData(table){
	var params = {
	    TableName: "amps-nupredicts-votes",
	    Item: {
	        "topic":"Presidential Election",
	        "timestamp":"4:23:22:22",
	        "netid":"abc047",
	        "vote": {
	            "question": {"title": "Is Donald Trump Gonna Win", "ID": 424242},
	            "answer": ["yes"]
	        }
	    }
	};
	console.log("Adding a new item...");
	docClient.put(params, function(err, data) {
	    if (err) {
	        console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
	    } else {
	        console.log("Added item:", JSON.stringify(data, null, 2));
	    }
	});
}

// shows all of the tables in the database
function listTables(){
	db.listTables(function (err, data){
   console.log('listTables',err,data);
	});
}	

// shows all of the items in a table given a table name
function showItems(table){
	var params = { 
	    TableName: table
	};

	docClient.scan(params, function(err, data) {
	    if (err)
	        console.log(JSON.stringify(err, null, 2));
	    else
	        console.log(JSON.stringify(data, null, 2));
	});
}
addData("hnng");