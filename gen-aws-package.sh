#!/usr/bin/env bash

[ ! -d "dist" ] && mkdir dist

# index.js
cp -r index.js dist/

# src
cp -r src dist/

# # creds
# cp -r Creds dist/

# # node modules
# cp -r node_modules dist/



fname="NUpredictsLambda.zip"
echo "create zip file..."
cd dist
zip -r "${fname}" *
cd ..
echo ""
echo "Done."
echo "dist/${fname}"
echo ""
echo "Upload the zip file to AWS Lambda via the AWS UI"

